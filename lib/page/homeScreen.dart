import 'dart:io';

import 'package:catdogapp/controller/tfController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tflite/tflite.dart';
import 'package:image_picker/image_picker.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _loadgin = true;

  File _image;

  List _output;

  final picker = ImagePicker();

  _classifyImage(File image) async {
    //? de esta forma se carga el modelo
    //? hay muchas opciones, pero el que se usara sera el OnImage

    var output = await Tflite.runModelOnImage(
        path: _image.path, //* la ruta de la imagen
        numResults:
            2, //* esta significaria numero de etiquetas que hay (gatos,perros)
        threshold: 0.5, //* estos 3 datos esta definidos en el video
        imageMean: 127.5,
        imageStd: 127.5);
    setState(() {
      _output = output;
      _loadgin = false;
    });
    print(image.path);
  }

  _loadModel() async {
    //? de esta forma cargamos nuestro modelo
    await Tflite.loadModel(
        model: 'assets/model_unquant.tflite', labels: 'assets/labels.text');
  }

  _pickImage() async {
    var pick = await picker.getImage(source: ImageSource.camera);
    if (pick == null) {
      return null;
    }
    setState(() {
      _image = File(pick.path);
    });
    _classifyImage(_image);
  }

  _pickImageGallery() async {
    var pick = await picker.getImage(source: ImageSource.gallery);
    if (pick == null) {
      return null;
    }
    setState(() {
      _image = File(pick.path);
    });
    _classifyImage(_image);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff101010),
      body: GetBuilder<TfController>(
        init: TfController(),
        builder: (bloc) => Container(
          padding: EdgeInsets.symmetric(horizontal: 24),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 50,
              ),
              Text(
                'TeachableMachine.com CNN',
                style: TextStyle(color: Color(0xffeeda28), fontSize: 18),
              ),
              SizedBox(
                height: 6,
              ),
              Text(
                'Detect dogs and cats',
                style: TextStyle(
                    color: Color(0xffe99600),
                    fontWeight: FontWeight.w500,
                    fontSize: 28),
              ),
              SizedBox(
                height: 40,
              ),
              SizedBox(
                height: 40,
              ),
              Center(
                child: _loadgin
                    ? Container(
                        alignment: Alignment.center,
                        height: 280,
                        child: Image.asset('assets/cat.png'))
                    : Container(
                        child: Column(
                          children: [
                            Container(
                              height: 250,
                              child: Image.file(_image),
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            _output != null
                                ? Text("${_output[0]['label']}",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20))
                                : Container()
                          ],
                        ),
                      ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RaisedButton(
                    onPressed: _pickImage,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 18),
                      child: Text(
                        'Take a photo',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    color: Colors.orange,
                  ),
                  RaisedButton(
                    onPressed: _pickImageGallery,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 8.0, vertical: 18),
                      child: Text(
                        'pick a photo',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    color: Colors.orange,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
