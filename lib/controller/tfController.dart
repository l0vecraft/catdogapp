import 'package:get/get.dart';
import 'package:tflite/tflite.dart';

class TfController extends GetxController {
  String _model = 'assets/model_unquant.tflite';
  String _label = 'assets/labels.txt';

  initModel() async {
    await Tflite.loadModel(model: _model, labels: _label);
    print('Modelo cargado');
  }

  @override
  void onReady() {
    super.onReady();
    initModel();
  }

  @override
  onClose() {
    super.onClose();
    Tflite.close();
  }
}
